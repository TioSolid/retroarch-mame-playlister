# RetroArch MAME Playlist Builder #

** Para ajuda em português, role até o final dessa página **

> English

The RetroArch MAME Playlist Builder is an app you can use to create "tailored" playlists for the RetroArch libretro frontend.

Since the MAME full ROMset has like 32k ROMs, RetroArch will just freeze if you try to create / use a playlist with all the games in it. With this app, you can create playlists for each MAME Category (Based on the Category.INI file) or even a single playlist with some categories removed (like Adult games, Slot Machines, etc).

All the builds (binaries) can be downloaded in the [Downloads Page](https://bitbucket.org/TioSolid/retroarch-mame-playlister/downloads);

## How to Use ##

### Input ###

1. Choose your `mameroms.dat` file (if you know how, you can export this file directly from the MAME emulator binary);
2. Choose the folder containing your MAME ROM set (*optional*);
3. Choose your `Category.ini` file (the release package comes with one);
4. Choose your RetroArch **CORES** folder (*optional*);
5. Click "GO!" and wait (this takes some time).

When the app finishes doing its thing, you will be presented with a list of games / clones and categories. Here, you can deselect individual games (or clones) and select the categories you want to **exclude** from your final playlist file (you can CTRL + Click to select multiple categories).

### Output ###

* **Output Folder**: Choose the folder where the playlists will be saved;
* **Override Paths To**: Optionally, you can choose to override the output path for the ROMSET and the RetroArch cores. This can be useful if you are using an Windows Computer to create the playlists but want to use them in a PiCade, Lakka or any other Linux machine for example. The path used in the override boxes will be prepended to the ROM files and cores in the playlist file;

### Core Options ###
* **Choose your destination OS** (this is only used to change the extension from the core files from `DLL` to `SO`);
* **Core File**: Optionally, choose a core file to use with your ROMS. If you choose `NONE (DETECT)`, RetroArch will prompt you for an emulator core for every ROM you try to run;

### Output Options ###

* **Playlist Type**: Self-explanatory. The output can be "one playlist per category" or "one playlist for all games";
* **Info Database**: RetroArch database used for the rom info (inside the frontend). Usually, only the `MAME2003` has the most up to date info (TIP: You can use MAME2003 or even FB Alpha here and run the games using the MAME core, for example);
* **No Clones**: If checked, the app will not process the clones from the list, only the main ROMs;

Now, you can just click GENERATE and wait for the playlists to be created.

## Notes ##

I tried to translate the app into Portuguese, but I gave up (I know nothing about localization in C#). You can send a pull request if you know to do that and I will gladly merge your commit into the project.

As always, you use this app at your own risk.

> Português

O RetroArch MAME Playlist Builder é um aplicativo usado para a criação de playlists customizados (para jogos do MAME) para o frontend RetroArch.

Como o ROMset completo do MAME tem mais de 32 mil jogos, se você criar um playlist com TODAS as roms e tentar usa-lo no RetroArch, ele irá travar. Utilizando esse app, você poderá criar playlists individuais para cada categoria do MAME, ou mesmo remover categorias que você não deseja (como jogos Adultos ou jogos de caça níqueis, etc).

As versões pré compiladas podem ser encontradas na [Área de downloads](https://bitbucket.org/TioSolid/retroarch-mame-playlister/downloads).

## Como Usar o Aplicativo ##

### Opções de Entrada (Input) ###

1. Escolha o caminho para o seu arquivo `mameroms.dat` na primeira caixa de opção;
2. Escolha a pasta contendo o seu ROM set do MAME (*opcional*);
3. Escolha o seu arquivo `Category.ini` (o pacote do aplicativo já acompanha esse arquivo);
4. Escolha a pasta onde ficam os **CORES** do seu RetroArch (*opcional*);
5. Clique em "GO!" e aguarde (esse processo pode levar um bom tempo);

Quando o app finalizar o processamento dos arquivos, uma lista contendo os jogos / clones e categorias será mostrada no lado direito do aplicativo. Caso deseje, você poderá desmarcar jogos individuais que deseje remover dos seus playlists, além de selecionar as categorias que você deseja excluír (para selecionar mais de uma categoria, basta segurar CTRL e clicar nas categorias desejadas).

### Opções de Saída (Output) ###

* **Output Folder**: Escolha a pasta onde os playlists serão salvos;
* **Override Paths To**: Opcionalmente, você pode escolher trocar o caminho das pastas de saída para o ROMSET e para a pasta contendo os cores do RetroArch. Isso é útil caso você esteja usando um computador Windows para gerar os playlists mas planeja usa-los em uma máquina Linux, como um PiCade ou Lakka, entre outros;

### Opções de Core do RetroArch (Core Options) ###

* **Choose your destination OS**: Usado somente para alterar a extensão dos arquivos de destino. Escolha a opção de acordo com o seu uso;
* **Core File**: Opcionalmente, você pode escolher um core que será utilizado pelo frontend para rodar suas ROMs. Caso você escolha a opção `NONE (DETECT)`, o RetroArch irá perguntar para você qual core deseja utilizar toda vez que você rodar um jogo;

### Opções de Saída (Output Options) ###

* **Playlist Type**: Você pode escolher que a saída do programa seja "um playlist por categoria" (primera opção) ou "um playlist com todos os jogos" (segunda opção);
* **Info Database**: Qual banco de dados do RetroArch será usado para consultar as informações de cada ROM dentro do frontend. Normalmente, o único banco com informações mais atualizadas é o `MAME2003`. Aqui, você poderá escolher um banco de dados diferente do core utilizado (por exemplo: Você poderá utilizar o banco de dados do FB Alpha porém rodar os jogos com o core do MAME);
* **No Clones**: Caso essa caixa seja selecionada, o aplicativo irá pular as ROMs consideradas clones e incluir nos playlists somente as ROMs principais;

Com tudo configurado, basta clicar em "GENERATE" e aguardar o processamento.

## Notes ##

Eu tentei traduzir o programa completo para Português, porém acabei desistindo pois manjo muito pouco de localização no C#. Caso você entenda disso e queira contribuir, basta mandar um pull request e eu terei prazer em incluir o seu trabalho no projeto.

E como sempre, utilize esse programa por conta e risco.
