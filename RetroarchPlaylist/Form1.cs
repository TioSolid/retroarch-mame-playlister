﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace RetroarchPlaylist
{
    public partial class Form1 : Form
    {
        Dictionary<string, List<string>> romByCategory = new Dictionary<string, List<string>>();
        Dictionary<string, List<string>> categoryByRom = new Dictionary<string, List<string>>();
        Dictionary<string, StreamWriter> playlistStreams = new Dictionary<string, StreamWriter>();
        Dictionary<string, string> playlistFilenames = new Dictionary<string, string>();

        string MessageBoxTitle = "RetroArch Playlister";

        public Form1()
        {
            InitializeComponent();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            txtRomsetORPath.Text = txtRomsetPath.Text;
            txtCoresORPath.Text = txtRetroarchCoresPath.Text;

            //Safety checks
            if (!File.Exists(txtMameDatPath.Text))
            {
                MessageBox.Show("Invalid Path to MAME DAT file", MessageBoxTitle);
                return;
            }

            if (!File.Exists(txtCategoryIniPath.Text))
            {
                MessageBox.Show("Invalid Path to Category.INI file", MessageBoxTitle);
                return;
            }

            //Category.ini Data
            using (StreamReader categoryFile = new StreamReader(txtCategoryIniPath.Text))
            {
                string currentCategoryName = "";
                while (!categoryFile.EndOfStream)
                {
                    string line = categoryFile.ReadLine();
                    if (line == "") { continue; }

                    //Faster than regex for sure
                    if (line[0] == '[') //Category, no rom name starts with [
                    {
                        line = line.Replace("[", "");
                        line = line.Replace("]", "");
                        currentCategoryName = line;

                        romByCategory.Add(currentCategoryName, new List<string>());
                    }
                    else
                    {
                        if (currentCategoryName != "")
                        {
                            romByCategory[currentCategoryName].Add(line);
                            if (!categoryByRom.ContainsKey(line)) { 
                                categoryByRom.Add(line, new List<string>());
                            }

                            categoryByRom[line].Add(currentCategoryName);
                        }
                    }
                }
            }

            foreach (KeyValuePair<string, List<string>> category in romByCategory)
            {
                if (category.Key == "FOLDER_SETTINGS" || category.Key == "ROOT_FOLDER") { continue; }
                listCategories.Items.Add(category.Key);
            }
            listCategories.Items.Add("Uncategorized");

            //MAME DAT ROMs
            Dictionary<string, List<string>> datRoms = new Dictionary<string, List<string>>();
            string xmlDatPath = txtMameDatPath.Text;
            XmlDocument mameDat = new XmlDocument();
            mameDat.Load(xmlDatPath);

            XmlNode rootNodeMameDat = mameDat.DocumentElement.SelectSingleNode("/datafile");

            //Status bar
            progressBarInput.Value = 0;
            progressBarInput.Minimum = 0;
            progressBarInput.Maximum = rootNodeMameDat.ChildNodes.Count;
            this.Text = $"[{progressBarInput.Value}%] RetroArch Playlister";

            foreach (XmlNode node in rootNodeMameDat)
            {
               if (node.Name == "game")
                {
                    if (node.Attributes["isdevice"]?.InnerText == "yes") { continue; } //Skip devices roms

                    string gameRom = node.Attributes["name"]?.InnerText;
                    string cloneOf = node.Attributes["cloneof"]?.InnerText;
                    string gameName = node.ChildNodes[0]?.InnerText; //description tag
                    string gameYear = node.ChildNodes[1]?.InnerText; //year tag
                    string gameManufacturer = node.ChildNodes[2]?.InnerText; //manufacturer tag

                    if (cloneOf == null) //Root game, not a clone
                    {
                        Dictionary<string, string> metaData = new Dictionary<string, string>();
                        metaData.Add("description", gameName);
                        metaData.Add("year", gameYear);
                        metaData.Add("manufacturer", gameManufacturer);
                        metaData.Add("romfile", gameRom);
                        if (categoryByRom.ContainsKey(gameRom))
                        {
                            metaData.Add("category", String.Join(",", categoryByRom[gameRom].ToArray()));
                        }
                        else
                        {
                            metaData.Add("category", "Uncategorized");
                        }

                        if (treeRoms.Nodes[gameRom] != null) //If already on the list (added by a clone)
                        {
                            //Update its description and metadata
                            treeRoms.Nodes[gameRom].Text = $"{gameName} [{gameRom}]";
                        }
                        else
                        {
                            //Add it to the list
                            treeRoms.Nodes.Add(gameRom, $"{gameName} [{gameRom}]");
                        }

                        treeRoms.Nodes[gameRom].Checked = true;
                        treeRoms.Nodes[gameRom].ToolTipText = $"{gameName} [{gameRom}]";
                        if (treeRoms.Nodes[gameRom].Tag is String)
                        {
                            //We need to update the category of the already added clones
                            foreach(TreeNode clone in treeRoms.Nodes[gameRom].Nodes)
                            {
                                Dictionary<string, string> cloneMetaData = (Dictionary <string, string>)clone.Tag;
                                cloneMetaData["category"] = metaData["category"];
                                clone.Tag = cloneMetaData;
                            }
                        }
                        treeRoms.Nodes[gameRom].Tag = metaData;
                    }
                    else //Add it as child since its a clone
                    {
                        if (treeRoms.Nodes[cloneOf] == null) 
                        {
                            //The root game is not on the list yet, add it
                            treeRoms.Nodes.Add(cloneOf, cloneOf);
                            treeRoms.Nodes[cloneOf].Checked = true;
                            treeRoms.Nodes[cloneOf].ToolTipText = cloneOf;
                            //We need to flag root games added by a clone
                            treeRoms.Nodes[cloneOf].Tag = "UPDATE";
                        }

                        Dictionary<string, string> metaData = new Dictionary<string, string>();
                        metaData.Add("description", gameName);
                        metaData.Add("year", gameYear);
                        metaData.Add("manufacturer", gameManufacturer);
                        metaData.Add("romfile", gameRom);
                        //Clones are always the same category as their base roms
                        if (treeRoms.Nodes[cloneOf].Tag is Dictionary<string, string>)
                        {
                            Dictionary<string, string> baseGameMetaData = (Dictionary<string, string>)treeRoms.Nodes[cloneOf].Tag;
                            metaData.Add("category", baseGameMetaData["category"]);
                        }
                        else
                        {
                            //This base game was added by a clone, will update its category later
                            metaData.Add("category", "");
                        }

                        treeRoms.Nodes[cloneOf].Nodes.Add(gameRom, $"{gameName} [{gameRom}]");
                        treeRoms.Nodes[cloneOf].Nodes[gameRom].Checked = true;
                        treeRoms.Nodes[cloneOf].Nodes[gameRom].ToolTipText = $"{gameName} [{gameRom}]";
                        treeRoms.Nodes[cloneOf].Nodes[gameRom].Tag = metaData;
                    }
                    //string gameDescription = gameName + " [" + gameRom + "]";
                    //listDat.Items.Add(gameDescription);
                }

                progressBarInput.Value++;
                int progress = progressBarInput.Value / (progressBarInput.Maximum / 100);
                this.Text = $"[{progress}%] RetroArch Playlister";
            }

            //MAME Roms
            /*
            string romsFolderPath = txtRomsetPath.Text;
            string[] roms = System.IO.Directory.GetFiles(romsFolderPath);

            foreach (string romPath in roms)
            {
                listRoms.Items.Add(romPath);
            }
            */


            /*  
            foreach (KeyValuePair<string, List<string>> category in categories)
            {
                foreach(string romname in category.Value)
                {
                    listCategories.Items.Add(romname + " [" + category.Key + "]");
                }
            } 
            */

            //RetroArch Cores
            comboCores.Items.Add("None (DETECT)");
            if (txtRetroarchCoresPath.Text.Length > 0) {             
                string[] cores = System.IO.Directory.GetFiles(txtRetroarchCoresPath.Text);
                foreach (string coreFilepath in cores)
                {
                    string coreFilename = Path.GetFileName(coreFilepath);
                    string coreFilenameNoExtension = coreFilename.Substring(0, coreFilename.Length - 4);
                    comboCores.Items.Add(coreFilenameNoExtension);
                }
            }
            comboCores.SelectedIndex = 0;

            comboPlaylistType.SelectedIndex = 0;
            comboDatabases.SelectedIndex = 0;

            btnGenerate.Enabled = true;
            progressBarInput.Value = 0;
            this.Text = "RetroArch Playlister";
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            //Safety Checks
            if (!Directory.Exists(txtOutputFolder.Text))
            {
                MessageBox.Show("The output folder is invalid or does not exist", MessageBoxTitle);
                return;
            }

            if (txtRomsetORPath.Text.Length < 1)
            {
                MessageBox.Show("The override folder for the ROMSET cant be empty", MessageBoxTitle);
                return;
            }

            if (comboPlaylistType.SelectedIndex == 0)
            {
                for (int i = 0; i < listCategories.Items.Count; i++)
                {
                    string categoryName = listCategories.Items[i].ToString();
                    //No need to create playlists for skipped categories
                    if (listCategories.SelectedItems.Contains(categoryName)) { continue; }

                    Regex invalidChars = new Regex("[\\/:*?\"<>|]");
                    string fileName = invalidChars.Replace(categoryName, "-");
                    fileName = $"MAME - {fileName}.lpl";
                    playlistStreams.Add(categoryName, new StreamWriter(txtOutputFolder.Text + Path.DirectorySeparatorChar + fileName));
                    playlistFilenames.Add(categoryName, fileName);
                }
            }
            else
            {
                playlistStreams.Add("MAME", new StreamWriter(txtOutputFolder.Text + Path.DirectorySeparatorChar + "MAME.lpl"));
                playlistFilenames.Add("MAME", "MAME.lpl");
            }
            
            foreach (TreeNode rom in treeRoms.Nodes)
            {
                AddRomToPlaylist(rom);
            }

            foreach (StreamWriter playlist in playlistStreams.Values)
            {
                playlist.Close();
            }
            playlistStreams.Clear();
            playlistFilenames.Clear();

            MessageBox.Show("DONE!", MessageBoxTitle);
        }

        private void AddRomToPlaylist(TreeNode rom)
        {
            Dictionary<string, string> metadata = (Dictionary <string, string>)rom.Tag;
            foreach (string categoryName in listCategories.SelectedItems)
            {
                //If the base rom has an excluded category, exclude all its clones (since they are from the same category too)
                if (metadata["category"].Contains(categoryName)) { return; }
            }

            if (rom.Checked)
            {
                string coreFilenamePath = "DETECT";
                if (comboCores.SelectedIndex > 0) { 
                    coreFilenamePath = txtCoresORPath.Text + Path.DirectorySeparatorChar + comboCores.SelectedItem.ToString();
                    if (radioWindows.Checked) { coreFilenamePath += ".dll"; } else { coreFilenamePath += ".so"; }
                }

                string databaseFilename = comboDatabases.SelectedItem.ToString();
                if (comboPlaylistType.SelectedIndex == 0)
                {
                    string[] categories = metadata["category"].Split(',');
                    //https://github.com/libretro/Lakka/wiki/Playlists
                    //https://github.com/libretro/libretro-database/tree/master/dat
                    foreach (string category in categories)
                    {
                        string romPath = txtRomsetORPath.Text + Path.DirectorySeparatorChar + metadata["romfile"] + ".zip";
                        playlistStreams[category].WriteLine(romPath); //Rom Path
                        playlistStreams[category].WriteLine($"{metadata["description"]}"); //Display Name
                        //playlistStreams[category].WriteLine("DETECT"); //Path to the libretro core (.so or .dll)
                        playlistStreams[category].WriteLine(coreFilenamePath); //Path to the libretro core (.so or .dll)
                        playlistStreams[category].WriteLine("DETECT"); //Core Display Name
                        playlistStreams[category].WriteLine("DETECT"); //CRC - Database link
                        //playlistStreams[category].WriteLine(playlistFilenames[category]); //Playlist name (actually CRC database name)
                        playlistStreams[category].WriteLine(databaseFilename); //Playlist name (actually CRC database name)
                    }
                }
                else
                {
                    string romPath = txtRomsetORPath.Text + Path.DirectorySeparatorChar + metadata["romfile"] + ".zip";
                    playlistStreams["MAME"].WriteLine(romPath); //Rom Path
                    playlistStreams["MAME"].WriteLine($"{metadata["description"]}"); //Display Name
                    //playlistStreams["MAME"].WriteLine("DETECT"); //Path to the libretro core (.so or .dll)
                    playlistStreams["MAME"].WriteLine(coreFilenamePath); //Path to the libretro core (.so or .dll)
                    playlistStreams["MAME"].WriteLine("DETECT"); //Core Display Name
                    playlistStreams["MAME"].WriteLine("DETECT"); //CRC - Database link
                    //playlistStreams["MAME"].WriteLine(playlistFilenames["MAME"]); //Playlist name (actually CRC database name)
                    playlistStreams["MAME"].WriteLine(databaseFilename); //Playlist name (actually CRC database name)
                }
            }

            if (!checkNoClones.Checked) { 
                foreach (TreeNode clone in rom.Nodes)
                {
                    AddRomToPlaylist(clone);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string categoryIniPath = Application.StartupPath + Path.DirectorySeparatorChar;
            if (File.Exists(categoryIniPath + "category.ini")) {
                txtCategoryIniPath.Text = categoryIniPath + "category.ini";
            }
            else if (File.Exists(categoryIniPath + "Category.ini"))
            {
                txtCategoryIniPath.Text = categoryIniPath + "Category.ini";
            }

            lblVersion.Text = "Version: " + Application.ProductVersion;

        }

        private void btnMameDatPath_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Choose the MAME DAT File To Be Used";
            openFileDialog.Filter = "MAME DAT Files|*.DAT";
            openFileDialog.FileName = "";

            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK) { txtMameDatPath.Text = openFileDialog.FileName; }
        }

        private void btnCategoryIniPath_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Choose the Category.INI File To Be Used";
            openFileDialog.Filter = "INI Files|*.INI";
            openFileDialog.FileName = "";

            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK) { txtCategoryIniPath.Text = openFileDialog.FileName; }
        }

        private void btnRomsetPath_Click(object sender, EventArgs e)
        {
            folderDialog.Description = "Choose The Folder Containing Your MAME ROMs";

            DialogResult result = folderDialog.ShowDialog();

            if (result == DialogResult.OK) {
                txtRomsetPath.Text = folderDialog.SelectedPath;
                txtRomsetORPath.Text = folderDialog.SelectedPath;
            }
        }

        private void btnRetroarchCoresPath_Click(object sender, EventArgs e)
        {
            folderDialog.Description = "Choose The Folder Containing Your RetroArch CORES";

            DialogResult result = folderDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                txtRetroarchCoresPath.Text = folderDialog.SelectedPath;
                txtCoresORPath.Text = folderDialog.SelectedPath;
            }
        }

        private void btnOutputFolder_Click(object sender, EventArgs e)
        {
            folderDialog.Description = "Choose The Folder Where the Playlists Will Be Saved";

            DialogResult result = folderDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                txtOutputFolder.Text = folderDialog.SelectedPath;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://bitbucket.org/TioSolid/retroarch-mame-playlister");
        }

        private void comboLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboLanguage.SelectedIndex)
            {
                case 0: //English
                    break;

                case 1: //Portuguese
                    break;
            }
        }
    }
}
