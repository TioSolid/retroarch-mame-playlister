﻿namespace RetroarchPlaylist
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupInput = new System.Windows.Forms.GroupBox();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBarInput = new System.Windows.Forms.ProgressBar();
            this.btnRetroarchCoresPath = new System.Windows.Forms.Button();
            this.btnCategoryIniPath = new System.Windows.Forms.Button();
            this.btnRomsetPath = new System.Windows.Forms.Button();
            this.btnMameDatPath = new System.Windows.Forms.Button();
            this.txtRetroarchCoresPath = new System.Windows.Forms.TextBox();
            this.lblRetroarchPath = new System.Windows.Forms.Label();
            this.txtCategoryIniPath = new System.Windows.Forms.TextBox();
            this.lblCategoryIni = new System.Windows.Forms.Label();
            this.btnGo = new System.Windows.Forms.Button();
            this.txtRomsetPath = new System.Windows.Forms.TextBox();
            this.lblRomset = new System.Windows.Forms.Label();
            this.txtMameDatPath = new System.Windows.Forms.TextBox();
            this.lblMameDat = new System.Windows.Forms.Label();
            this.treeRoms = new System.Windows.Forms.TreeView();
            this.GroupOutput = new System.Windows.Forms.GroupBox();
            this.comboLanguage = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.groupOutputPathOptions = new System.Windows.Forms.GroupBox();
            this.btnOutputFolder = new System.Windows.Forms.Button();
            this.txtCoresORPath = new System.Windows.Forms.TextBox();
            this.lblORRetroarch = new System.Windows.Forms.Label();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.lblOutputFolder = new System.Windows.Forms.Label();
            this.txtRomsetORPath = new System.Windows.Forms.TextBox();
            this.lblORRomset = new System.Windows.Forms.Label();
            this.groupOutputCoreOptions = new System.Windows.Forms.GroupBox();
            this.lblCoreFile = new System.Windows.Forms.Label();
            this.comboCores = new System.Windows.Forms.ComboBox();
            this.lblOS = new System.Windows.Forms.Label();
            this.radioLinux = new System.Windows.Forms.RadioButton();
            this.radioWindows = new System.Windows.Forms.RadioButton();
            this.groupOutputOutputOptions = new System.Windows.Forms.GroupBox();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.lblPlaylistType = new System.Windows.Forms.Label();
            this.comboDatabases = new System.Windows.Forms.ComboBox();
            this.checkNoClones = new System.Windows.Forms.CheckBox();
            this.comboPlaylistType = new System.Windows.Forms.ComboBox();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.lblExcludedCategories = new System.Windows.Forms.Label();
            this.listCategories = new System.Windows.Forms.ListBox();
            this.lblGamesClones = new System.Windows.Forms.Label();
            this.folderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupInput.SuspendLayout();
            this.GroupOutput.SuspendLayout();
            this.groupOutputPathOptions.SuspendLayout();
            this.groupOutputCoreOptions.SuspendLayout();
            this.groupOutputOutputOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupInput
            // 
            this.groupInput.Controls.Add(this.lblProgress);
            this.groupInput.Controls.Add(this.progressBarInput);
            this.groupInput.Controls.Add(this.btnRetroarchCoresPath);
            this.groupInput.Controls.Add(this.btnCategoryIniPath);
            this.groupInput.Controls.Add(this.btnRomsetPath);
            this.groupInput.Controls.Add(this.btnMameDatPath);
            this.groupInput.Controls.Add(this.txtRetroarchCoresPath);
            this.groupInput.Controls.Add(this.lblRetroarchPath);
            this.groupInput.Controls.Add(this.txtCategoryIniPath);
            this.groupInput.Controls.Add(this.lblCategoryIni);
            this.groupInput.Controls.Add(this.btnGo);
            this.groupInput.Controls.Add(this.txtRomsetPath);
            this.groupInput.Controls.Add(this.lblRomset);
            this.groupInput.Controls.Add(this.txtMameDatPath);
            this.groupInput.Controls.Add(this.lblMameDat);
            resources.ApplyResources(this.groupInput, "groupInput");
            this.groupInput.Name = "groupInput";
            this.groupInput.TabStop = false;
            // 
            // lblProgress
            // 
            resources.ApplyResources(this.lblProgress, "lblProgress");
            this.lblProgress.Name = "lblProgress";
            // 
            // progressBarInput
            // 
            resources.ApplyResources(this.progressBarInput, "progressBarInput");
            this.progressBarInput.Name = "progressBarInput";
            this.progressBarInput.Step = 1;
            this.progressBarInput.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // btnRetroarchCoresPath
            // 
            resources.ApplyResources(this.btnRetroarchCoresPath, "btnRetroarchCoresPath");
            this.btnRetroarchCoresPath.Name = "btnRetroarchCoresPath";
            this.btnRetroarchCoresPath.UseVisualStyleBackColor = true;
            this.btnRetroarchCoresPath.Click += new System.EventHandler(this.btnRetroarchCoresPath_Click);
            // 
            // btnCategoryIniPath
            // 
            resources.ApplyResources(this.btnCategoryIniPath, "btnCategoryIniPath");
            this.btnCategoryIniPath.Name = "btnCategoryIniPath";
            this.btnCategoryIniPath.UseVisualStyleBackColor = true;
            this.btnCategoryIniPath.Click += new System.EventHandler(this.btnCategoryIniPath_Click);
            // 
            // btnRomsetPath
            // 
            resources.ApplyResources(this.btnRomsetPath, "btnRomsetPath");
            this.btnRomsetPath.Name = "btnRomsetPath";
            this.btnRomsetPath.UseVisualStyleBackColor = true;
            this.btnRomsetPath.Click += new System.EventHandler(this.btnRomsetPath_Click);
            // 
            // btnMameDatPath
            // 
            resources.ApplyResources(this.btnMameDatPath, "btnMameDatPath");
            this.btnMameDatPath.Name = "btnMameDatPath";
            this.btnMameDatPath.UseVisualStyleBackColor = true;
            this.btnMameDatPath.Click += new System.EventHandler(this.btnMameDatPath_Click);
            // 
            // txtRetroarchCoresPath
            // 
            resources.ApplyResources(this.txtRetroarchCoresPath, "txtRetroarchCoresPath");
            this.txtRetroarchCoresPath.Name = "txtRetroarchCoresPath";
            // 
            // lblRetroarchPath
            // 
            resources.ApplyResources(this.lblRetroarchPath, "lblRetroarchPath");
            this.lblRetroarchPath.Name = "lblRetroarchPath";
            // 
            // txtCategoryIniPath
            // 
            resources.ApplyResources(this.txtCategoryIniPath, "txtCategoryIniPath");
            this.txtCategoryIniPath.Name = "txtCategoryIniPath";
            // 
            // lblCategoryIni
            // 
            resources.ApplyResources(this.lblCategoryIni, "lblCategoryIni");
            this.lblCategoryIni.Name = "lblCategoryIni";
            // 
            // btnGo
            // 
            resources.ApplyResources(this.btnGo, "btnGo");
            this.btnGo.Name = "btnGo";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // txtRomsetPath
            // 
            resources.ApplyResources(this.txtRomsetPath, "txtRomsetPath");
            this.txtRomsetPath.Name = "txtRomsetPath";
            // 
            // lblRomset
            // 
            resources.ApplyResources(this.lblRomset, "lblRomset");
            this.lblRomset.Name = "lblRomset";
            // 
            // txtMameDatPath
            // 
            resources.ApplyResources(this.txtMameDatPath, "txtMameDatPath");
            this.txtMameDatPath.Name = "txtMameDatPath";
            // 
            // lblMameDat
            // 
            resources.ApplyResources(this.lblMameDat, "lblMameDat");
            this.lblMameDat.Name = "lblMameDat";
            // 
            // treeRoms
            // 
            this.treeRoms.CheckBoxes = true;
            this.treeRoms.FullRowSelect = true;
            resources.ApplyResources(this.treeRoms, "treeRoms");
            this.treeRoms.Name = "treeRoms";
            this.treeRoms.ShowNodeToolTips = true;
            // 
            // GroupOutput
            // 
            this.GroupOutput.Controls.Add(this.comboLanguage);
            this.GroupOutput.Controls.Add(this.label1);
            this.GroupOutput.Controls.Add(this.linkLabel1);
            this.GroupOutput.Controls.Add(this.lblVersion);
            this.GroupOutput.Controls.Add(this.groupOutputPathOptions);
            this.GroupOutput.Controls.Add(this.groupOutputCoreOptions);
            this.GroupOutput.Controls.Add(this.groupOutputOutputOptions);
            this.GroupOutput.Controls.Add(this.btnGenerate);
            resources.ApplyResources(this.GroupOutput, "GroupOutput");
            this.GroupOutput.Name = "GroupOutput";
            this.GroupOutput.TabStop = false;
            // 
            // comboLanguage
            // 
            this.comboLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.comboLanguage, "comboLanguage");
            this.comboLanguage.FormattingEnabled = true;
            this.comboLanguage.Items.AddRange(new object[] {
            resources.GetString("comboLanguage.Items"),
            resources.GetString("comboLanguage.Items1")});
            this.comboLanguage.Name = "comboLanguage";
            this.comboLanguage.SelectedIndexChanged += new System.EventHandler(this.comboLanguage_SelectedIndexChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // linkLabel1
            // 
            resources.ApplyResources(this.linkLabel1, "linkLabel1");
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.TabStop = true;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // lblVersion
            // 
            resources.ApplyResources(this.lblVersion, "lblVersion");
            this.lblVersion.Name = "lblVersion";
            // 
            // groupOutputPathOptions
            // 
            this.groupOutputPathOptions.Controls.Add(this.btnOutputFolder);
            this.groupOutputPathOptions.Controls.Add(this.txtCoresORPath);
            this.groupOutputPathOptions.Controls.Add(this.lblORRetroarch);
            this.groupOutputPathOptions.Controls.Add(this.txtOutputFolder);
            this.groupOutputPathOptions.Controls.Add(this.lblOutputFolder);
            this.groupOutputPathOptions.Controls.Add(this.txtRomsetORPath);
            this.groupOutputPathOptions.Controls.Add(this.lblORRomset);
            resources.ApplyResources(this.groupOutputPathOptions, "groupOutputPathOptions");
            this.groupOutputPathOptions.Name = "groupOutputPathOptions";
            this.groupOutputPathOptions.TabStop = false;
            // 
            // btnOutputFolder
            // 
            resources.ApplyResources(this.btnOutputFolder, "btnOutputFolder");
            this.btnOutputFolder.Name = "btnOutputFolder";
            this.btnOutputFolder.UseVisualStyleBackColor = true;
            this.btnOutputFolder.Click += new System.EventHandler(this.btnOutputFolder_Click);
            // 
            // txtCoresORPath
            // 
            resources.ApplyResources(this.txtCoresORPath, "txtCoresORPath");
            this.txtCoresORPath.Name = "txtCoresORPath";
            // 
            // lblORRetroarch
            // 
            resources.ApplyResources(this.lblORRetroarch, "lblORRetroarch");
            this.lblORRetroarch.Name = "lblORRetroarch";
            // 
            // txtOutputFolder
            // 
            resources.ApplyResources(this.txtOutputFolder, "txtOutputFolder");
            this.txtOutputFolder.Name = "txtOutputFolder";
            // 
            // lblOutputFolder
            // 
            resources.ApplyResources(this.lblOutputFolder, "lblOutputFolder");
            this.lblOutputFolder.Name = "lblOutputFolder";
            // 
            // txtRomsetORPath
            // 
            resources.ApplyResources(this.txtRomsetORPath, "txtRomsetORPath");
            this.txtRomsetORPath.Name = "txtRomsetORPath";
            // 
            // lblORRomset
            // 
            resources.ApplyResources(this.lblORRomset, "lblORRomset");
            this.lblORRomset.Name = "lblORRomset";
            // 
            // groupOutputCoreOptions
            // 
            this.groupOutputCoreOptions.Controls.Add(this.lblCoreFile);
            this.groupOutputCoreOptions.Controls.Add(this.comboCores);
            this.groupOutputCoreOptions.Controls.Add(this.lblOS);
            this.groupOutputCoreOptions.Controls.Add(this.radioLinux);
            this.groupOutputCoreOptions.Controls.Add(this.radioWindows);
            resources.ApplyResources(this.groupOutputCoreOptions, "groupOutputCoreOptions");
            this.groupOutputCoreOptions.Name = "groupOutputCoreOptions";
            this.groupOutputCoreOptions.TabStop = false;
            // 
            // lblCoreFile
            // 
            resources.ApplyResources(this.lblCoreFile, "lblCoreFile");
            this.lblCoreFile.Name = "lblCoreFile";
            // 
            // comboCores
            // 
            this.comboCores.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCores.FormattingEnabled = true;
            resources.ApplyResources(this.comboCores, "comboCores");
            this.comboCores.Name = "comboCores";
            // 
            // lblOS
            // 
            resources.ApplyResources(this.lblOS, "lblOS");
            this.lblOS.Name = "lblOS";
            // 
            // radioLinux
            // 
            resources.ApplyResources(this.radioLinux, "radioLinux");
            this.radioLinux.Name = "radioLinux";
            this.radioLinux.UseVisualStyleBackColor = true;
            // 
            // radioWindows
            // 
            resources.ApplyResources(this.radioWindows, "radioWindows");
            this.radioWindows.Checked = true;
            this.radioWindows.Name = "radioWindows";
            this.radioWindows.TabStop = true;
            this.radioWindows.UseVisualStyleBackColor = true;
            // 
            // groupOutputOutputOptions
            // 
            this.groupOutputOutputOptions.Controls.Add(this.lblDatabase);
            this.groupOutputOutputOptions.Controls.Add(this.lblPlaylistType);
            this.groupOutputOutputOptions.Controls.Add(this.comboDatabases);
            this.groupOutputOutputOptions.Controls.Add(this.checkNoClones);
            this.groupOutputOutputOptions.Controls.Add(this.comboPlaylistType);
            resources.ApplyResources(this.groupOutputOutputOptions, "groupOutputOutputOptions");
            this.groupOutputOutputOptions.Name = "groupOutputOutputOptions";
            this.groupOutputOutputOptions.TabStop = false;
            // 
            // lblDatabase
            // 
            resources.ApplyResources(this.lblDatabase, "lblDatabase");
            this.lblDatabase.Name = "lblDatabase";
            // 
            // lblPlaylistType
            // 
            resources.ApplyResources(this.lblPlaylistType, "lblPlaylistType");
            this.lblPlaylistType.Name = "lblPlaylistType";
            // 
            // comboDatabases
            // 
            this.comboDatabases.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDatabases.FormattingEnabled = true;
            this.comboDatabases.Items.AddRange(new object[] {
            resources.GetString("comboDatabases.Items"),
            resources.GetString("comboDatabases.Items1"),
            resources.GetString("comboDatabases.Items2")});
            resources.ApplyResources(this.comboDatabases, "comboDatabases");
            this.comboDatabases.Name = "comboDatabases";
            // 
            // checkNoClones
            // 
            resources.ApplyResources(this.checkNoClones, "checkNoClones");
            this.checkNoClones.Name = "checkNoClones";
            this.checkNoClones.UseVisualStyleBackColor = true;
            // 
            // comboPlaylistType
            // 
            this.comboPlaylistType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPlaylistType.FormattingEnabled = true;
            this.comboPlaylistType.Items.AddRange(new object[] {
            resources.GetString("comboPlaylistType.Items"),
            resources.GetString("comboPlaylistType.Items1")});
            resources.ApplyResources(this.comboPlaylistType, "comboPlaylistType");
            this.comboPlaylistType.Name = "comboPlaylistType";
            // 
            // btnGenerate
            // 
            resources.ApplyResources(this.btnGenerate, "btnGenerate");
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // lblExcludedCategories
            // 
            resources.ApplyResources(this.lblExcludedCategories, "lblExcludedCategories");
            this.lblExcludedCategories.Name = "lblExcludedCategories";
            // 
            // listCategories
            // 
            this.listCategories.FormattingEnabled = true;
            resources.ApplyResources(this.listCategories, "listCategories");
            this.listCategories.Name = "listCategories";
            this.listCategories.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listCategories.Sorted = true;
            // 
            // lblGamesClones
            // 
            resources.ApplyResources(this.lblGamesClones, "lblGamesClones");
            this.lblGamesClones.Name = "lblGamesClones";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblGamesClones);
            this.Controls.Add(this.listCategories);
            this.Controls.Add(this.GroupOutput);
            this.Controls.Add(this.lblExcludedCategories);
            this.Controls.Add(this.treeRoms);
            this.Controls.Add(this.groupInput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupInput.ResumeLayout(false);
            this.groupInput.PerformLayout();
            this.GroupOutput.ResumeLayout(false);
            this.GroupOutput.PerformLayout();
            this.groupOutputPathOptions.ResumeLayout(false);
            this.groupOutputPathOptions.PerformLayout();
            this.groupOutputCoreOptions.ResumeLayout(false);
            this.groupOutputCoreOptions.PerformLayout();
            this.groupOutputOutputOptions.ResumeLayout(false);
            this.groupOutputOutputOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCategoryIniPath;
        private System.Windows.Forms.Label lblCategoryIni;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.TextBox txtRomsetPath;
        private System.Windows.Forms.Label lblRomset;
        private System.Windows.Forms.TextBox txtMameDatPath;
        private System.Windows.Forms.TreeView treeRoms;
        private System.Windows.Forms.Label lblMameDat;
        private System.Windows.Forms.GroupBox groupInput;
        private System.Windows.Forms.GroupBox GroupOutput;
        private System.Windows.Forms.CheckBox checkNoClones;
        private System.Windows.Forms.Label lblExcludedCategories;
        private System.Windows.Forms.ComboBox comboPlaylistType;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.ListBox listCategories;
        private System.Windows.Forms.TextBox txtRetroarchCoresPath;
        private System.Windows.Forms.Label lblRetroarchPath;
        private System.Windows.Forms.ComboBox comboDatabases;
        private System.Windows.Forms.GroupBox groupOutputPathOptions;
        private System.Windows.Forms.TextBox txtCoresORPath;
        private System.Windows.Forms.Label lblORRetroarch;
        private System.Windows.Forms.TextBox txtOutputFolder;
        private System.Windows.Forms.Label lblOutputFolder;
        private System.Windows.Forms.TextBox txtRomsetORPath;
        private System.Windows.Forms.Label lblORRomset;
        private System.Windows.Forms.GroupBox groupOutputCoreOptions;
        private System.Windows.Forms.Label lblCoreFile;
        private System.Windows.Forms.ComboBox comboCores;
        private System.Windows.Forms.Label lblOS;
        private System.Windows.Forms.RadioButton radioLinux;
        private System.Windows.Forms.RadioButton radioWindows;
        private System.Windows.Forms.GroupBox groupOutputOutputOptions;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.Label lblPlaylistType;
        private System.Windows.Forms.Label lblGamesClones;
        private System.Windows.Forms.Button btnRetroarchCoresPath;
        private System.Windows.Forms.Button btnCategoryIniPath;
        private System.Windows.Forms.Button btnRomsetPath;
        private System.Windows.Forms.Button btnMameDatPath;
        private System.Windows.Forms.FolderBrowserDialog folderDialog;
        private System.Windows.Forms.Button btnOutputFolder;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.ProgressBar progressBarInput;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.ComboBox comboLanguage;
        private System.Windows.Forms.Label label1;
    }
}

